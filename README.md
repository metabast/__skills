Compétences v.0.2
------------------
	Programmation
		AS3 9/10
		JS 7/10
		html5 5/10
		css3 4/10
		PHP 7/10
		Python 5/10
		C# 4/10
		Java 4/10
		Git 7/10

	3D
		Modélisation 6/10
		Animation 6/10
		Unity3d 5/10

	2D
		Photoshop 7/10
		Illustrator 6/10
		Dessin 5/10

	Vidéo
		PostProd 6/10

	Linux Debian - Ubuntu
		Général 6/10	


Savoir faire
-------------
	Applications
		iphone/ipad/air 8/10
		Publication apple store 7/10
		Publication google store 7/10
		nodewebkit 4/10

	Web
		Wordpress 7/10
		backend symfony 6/10
		Ajax 8/10
		backbones 6/10
		jquery 8/10

	Optimisations médias
		Flux vidéos 8/10
		Images 9/10
		texture atlas / spritesheet 9/10

	Ergonomie 7/10

	Animation 7/10

	Serveur
		Installation serveur linux 7/10
		Backup sur autre serveur 7/10
		Sécurité 5/10
		Serveur PHP 7/10
		Serveur NodeJS 6/10
		Serveur Python 5/10

	Culture digitale 6/10

	Culture jeux vidéo 8/10

	Sensibilité musique 9/10

	UX 7/10

	Veille technologique
		flashdaily.net
		daily.js
		creativeapplications.net

IDE / EDITEUR / LOGICIELS / connus et utilisés
--------------------------
	FDT
	Flash
	Flash builder
	Crocus Modeller
	Phpstorm
	SublimeText2
	Maya
	Blender
	AfterEffect
	Photoshop
	Illustrator
	Unity3d
	VirtualBox
	TexturePacker
	nodejs / npm

FRAMEWORKS / LIBRAIRIES
------------------------
	AS3
		MVC Express
		Robotlegs
		Greensock
		Away3d
	
	PHP
		Symfony
	
	JS
		Jquery
		Backbones

	NODEJS
		grunt
		bower
		express.js
		socket.io



	
